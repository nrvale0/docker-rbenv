FROM ubuntu:14.04
MAINTAINER Nathan Valentine <nrvale0@gmail.com | nathan@nextdoor.com >

ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_NONINTERACTIVE_SEEN true

RUN apt-get update && apt-get -y upgrade
RUN apt-get install -y build-essential subversion autoconf libpq-dev \
    libsqlite3-dev libmysqlclient-dev zlib1g-dev libssl-dev \
    libreadline-dev libyaml-dev libxml2-dev libxslt-dev git python3 zsh curl
RUN apt-get clean

ENV RBENV_URI https://github.com/sstephenson/rbenv.git
ENV RUBY_BUILD_URI https://github.com/sstephenson/ruby-build.git
ENV RUBY_VERSION 2.2.3
ENV USER root
ENV UID 0
ENV HOME /root
ENV SHELL /bin/zsh

ADD ./scripts/prep /prep

ENTRYPOINT [ "/prep" ]
